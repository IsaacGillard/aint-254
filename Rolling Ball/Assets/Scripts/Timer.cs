﻿using UnityEngine;
using System.Collections;
using System.IO;
using UnityEngine.UI;

public class Timer : MonoBehaviour {

    float Clock = 0.0f;

    [SerializeField]
    private Text timerLabel;

    private float time;
    private bool isFinished = false;

    void Update()
    {
        if (isFinished == false)
        {
            time += Time.deltaTime;

            var minutes = time / 60;
            var seconds = time % 60;
            var fraction = (time * 100) % 100;

            timerLabel.text = string.Format("{0:00} : {1:00} : {2:000}", minutes, seconds, fraction);
        }
    }

    public void FinalTime()
    {
        isFinished = true;
    }

}
