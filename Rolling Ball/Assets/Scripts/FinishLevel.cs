﻿using UnityEngine;
using System.Collections;

public class FinishLevel : MonoBehaviour {


    public Transform canvas;

    public Transform player;

    private Timer timer;

    public GameObject m_ui;


	// Use this for initialization
	void Start () {
        timer = m_ui.GetComponent<Timer>();
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("LevelEnd"))
        {
            LevelFinish();
        }
    }

    public void LevelFinish()
    {
        if(canvas.gameObject.activeInHierarchy == false)
        {
            canvas.gameObject.SetActive(true);
            timer.FinalTime();

        }
    }


}
