﻿using UnityEngine;
using System.Collections;

public class PauseMenu : MonoBehaviour {

    public Transform Canvas;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if(Canvas.gameObject.activeInHierarchy == false)
            {
                Canvas.gameObject.SetActive(true);
                Time.timeScale = 0;
            }
            else
            {
                Canvas.gameObject.SetActive(false);
                Time.timeScale = 1;
            }
        }
	
	}
}
