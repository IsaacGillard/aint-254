﻿using UnityEngine;
using System.Collections;

public class ColourChange : MonoBehaviour {

    public Material[] material;
    Renderer rend;
    private int colour = 0;
    public GameObject greenWall;
    public GameObject redWall;
    public GameObject yellowWall;
    public GameObject greenFloor;
    public GameObject redFloor;
    public GameObject yellowFloor;


    // Use this for initialization
    void Start ()
    {
        rend = GetComponent<Renderer>();
        rend.enabled = true;
        rend.sharedMaterial = material[0];
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Red"))
        {
            rend.sharedMaterial = material[1];
            colour = 1;
            greenWall.SetActive(true);
            redWall.SetActive(false);
            yellowFloor.SetActive(true);
            greenFloor.SetActive(true);
            redFloor.SetActive(false);
            yellowWall.SetActive(true);
        }

        else if (other.gameObject.CompareTag("Yellow"))
        {
            rend.sharedMaterial = material[2];
            colour = 2;
            greenWall.SetActive(true);
            redWall.SetActive(true);
            yellowWall.SetActive(false);
            greenFloor.SetActive(true);
            redFloor.SetActive(true);
            yellowFloor.SetActive(false);
        }

        else if (other.gameObject.CompareTag("Green"))
        {
            rend.sharedMaterial = material[3];
            colour = 3;
            greenWall.SetActive(false);
            redWall.SetActive(true);
            yellowWall.SetActive(true);
            greenFloor.SetActive(false);
            redFloor.SetActive(true);
            yellowFloor.SetActive(true);
        }
    }
}
