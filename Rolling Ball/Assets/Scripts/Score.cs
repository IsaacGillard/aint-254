﻿using UnityEngine;
using System.Collections;
using System.IO;
using UnityEngine.UI;

public class Score : MonoBehaviour {

    private int score = 0;
    private int maxScore;
    public GameObject coinWall;

    [SerializeField]
    private Text scoreCount;

    // Use this for initialization
    void Start () {

        score = 0;
        scoreCount.text = string.Format("{0} : {1}", score, maxScore);

    }
	
    public void AddScore()
    {
        score += 1;
        scoreCount.text = string.Format("{0} : {1}", score, maxScore);
        Debug.Log("Score added");

        if(score >=3)
        {
            coinWall.SetActive(false);
        }
    }
}
