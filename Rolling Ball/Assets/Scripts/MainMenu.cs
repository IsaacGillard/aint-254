﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {

    public Transform levelSelect;
    public Transform mainMenu;


	// Use this for initialization
	void Start () {
	
	}

    public void LevelSelectMenu()
    {
        if (levelSelect.gameObject.activeInHierarchy == false)
        {
            levelSelect.gameObject.SetActive(true);
            mainMenu.gameObject.SetActive(false);
        }
    }

    public void Exit()
    {
        Application.Quit();
    }
}
