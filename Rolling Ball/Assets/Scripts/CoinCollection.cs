﻿using UnityEngine;
using System.Collections;

public class CoinCollection : MonoBehaviour
{

    private Score score;
    public GameObject coinCollect;

    void Start()
    {
        score = coinCollect.GetComponent<Score>();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            
            score.AddScore();
            Destroy(gameObject);

        }
    }
}
