﻿using UnityEngine;
using System.Collections;

public class DodgePlayer : MonoBehaviour
{

    private DodgeMovement dodgemovement;
    public GameObject m_ui;

    // Use this for initialization
    void Start()
    {
        dodgemovement = m_ui.GetComponent<DodgeMovement>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("DodgeStart"))
        {

            dodgemovement.StartMovement();

        }
    }
}
